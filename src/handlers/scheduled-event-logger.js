/**
 * TheFwGuy - October 2020
 * A Lambda function that logs the payload received from a CloudWatch scheduled event.
 */
let localStorage = require('store');
const api   = require('../utils/api');
const csv   = require('../utils/csv');
const utils = require('../utils/utils');
const dflt  = require('../utils/defaults');

// Constant assignment - modify them here - don't go in the code
// Name of the CSV file for UOM conversion in the S3 bucket
const uomConversionFilename = "uomConversion.csv";
const tempCsvResultFileName = "/tmp/csvoutput.csv";
const localUomCsvFileName = "/tmp/uomcsv.csv";

// Debug flags - these are global variables.
// They are forced to default (false) at every run unless the system variable SAV_DEBUG is set
let systemDebug = false;
let mainDebug   = false;
let apiDebug    = false;
let csvDebug    = false;
let uomDebug    = false;
let stateDebug  = false;

// Flag to indicate if a UOM conversion table exists
let uomConversionAvailable = false;     // Flag to indicate if exists the UOM conversion table
let uomConversionTable = [];            // Array to store conversion table
let totalProductsCount = 0;             // Counter for all retrieved products
let dataSource = false;                 // Flag to define if use the Inventory API retrieval or the detailed one
let generatedCsvFilename = "";          // Generated CSV File name

// State machine to handle the process - the state machine is updated by the phases
// When the lambda time is close to 250 ms the state machine is forced out
// 0  --> start
// 1  --> wait
// 2  --> load UOM conversion
// 3  --> read token API
// 4  --> read sites API
// 5  --> read Inventory API legacy
// 6  --> read Inventory API detailed
// 7  --> UOM conversion
// 8  --> CSV preparation
// 9  --> Sending to S3 bucket
// 10 --> Error ! Abort !
// 11 --> Gracefully exit

const states =
    {"start":0, "wait":1, "loadUom":2,
     "apiToken":3, "apiSites":4, "apiInventoryL":5, "apiInventoryD":6,
     "uomConv":7, "csvPrep":8, "sendS3":9, "Abort":10, "endOp":11};
Object.freeze(states);

let activity = states.start;

exports.scheduledEventLoggerHandler = async (event, context) => {
    // Store here the retrieved access token from the getToken function
    let apiAnswer = null;
    let apiToken = null;
    let csvAnswer = null;

    // All log statements are written to CloudWatch by default. For more information, see
    // https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-logging.html
    console.info("DailyInventorySnapshot triggered")

    // Set up debug (if any)
    // Force always default :
    systemDebug = false;
    mainDebug   = false;
    apiDebug    = false;
    csvDebug    = false;
    uomDebug    = false;
    stateDebug  = false;

    switch (dflt.retDebug()) {
        case '0':         // No debug - leave global setting
        default:
            break;
        case '1':         // Only system variables debug
            systemDebug = true;
            break;
        case '2':         // Only main messages debug
            mainDebug = true;
            break;
        case '3':         // Only API debug
            apiDebug = true;
            break;
        case '4':         // Only CSV debug
            csvDebug = true;
            break;
        case '5':         // Only UOM debug
            uomDebug = true;
            break;
        case '6':         // Only State machine debug
            stateDebug = true;
            break;
        case '7':         // Enable ALL debug !
            systemDebug = true;
            mainDebug   = true;
            apiDebug    = true;
            csvDebug    = true;
            uomDebug    = true;
            stateDebug  = true;
            break;
    }

    if (systemDebug) {
        // Display important System variables
        dflt.displayGlobalAssignment();

        console.log('Remaining time           =>', context.getRemainingTimeInMillis() + " ms");
//        console.log('functionName             =>', context.functionName);
        console.log('AWSrequestID             =>', context.awsRequestId);
        console.log('------------------------------------------------\n');
    }

    // ---------------------------------- Variables initialization -----------------------------------------------------
    // Set flag to indicate data source
    //  - false --> legacy Inventory API
    //  - true  --> detailed Inventory API
    dataSource = dflt.retApiInventoryDetails().localeCompare("true") === 0;
    // Reset total product counter
    totalProductsCount = 0;

    //
    // ------------------------------  STATE MACHINE MANAGEMENT  -------------------------------------------------------
    //

    // Get out from this loop only when state machine reach end or for Abort
    while (activity !== states.endOp) {
        switch (activity) {
            case states.start:          // --- START  ------------------------------------------------------------------
                if (stateDebug) console.log('==> state machine - START');
            default:
                activity = states.loadUom;   // Force start process reading the UOM conversion file
                break;

            case states.wait:           // --- WAIT --------------------------------------------------------------------
//                if (stateDebug) console.log('==> state machine - WAIT');
                // Check remaining time
                if (utils.isTimeToGoodBye(context)) {
                    if (stateDebug) console.log('GOODBYE !');
                    activity = states.Abort;
                }
                break;

            case states.loadUom:        // --- UOM Conversion table retrieval ------------------------------------------
                if (stateDebug) console.log('==> state machine - LOAD UOM');
                activity = states.wait;  // Force next state to wait until response change that

                // Reading CSV file containing UOM conversion
                // This state will change on the same next state because is not changing the main flow
                // i.e. if the UOM conversion table is not present the process continue
                await downloadFileS3(
                    dflt.retBucketName(),
                    dflt.retBucketRegion(),
                    uomConversionFilename,
                    dflt.retCsvBucketPathUomConversion()
                ).then(response => {
                    if (uomDebug) {
                        console.log(`UOM conversion table loaded and ready`);
                    }
                    uomConversionAvailable = true;
                    activity = states.apiToken;
                }, reject => {
                    if (uomDebug) {
                        console.error(reject);     // Display reject message
                    }
                    console.info("UOM conversion CSV file not found !");
                    uomConversionTable.length = 0;
                    uomConversionAvailable = false;
                    activity = states.apiToken;
                })
                break;

            case states.apiToken:       // ---  Read API token ---------------------------------------------------------
                if (stateDebug) console.log('==> state machine - API TOKEN');
                activity = states.wait;  // Force next state to wait until response change that

                await api.getToken(apiDebug,
                    dflt.retApiEndpoint(),
                    dflt.retApiSiteIdLogin()).then(response => {
                    // extract the access_token to be used on the other APIs
                    apiToken = (JSON.parse(response)).access_token;
                    if (mainDebug) {
                        console.log("Retrieved the access token : " + apiToken);
                    } else {
                        console.info("Retrieved the access token");
                    }
                    activity = states.apiSites;
                }, reject => {
                    console.error("Unable to login to Savant server ! No CSV file prepared !");
                    activity = states.Abort;
                });
                break;

            case states.apiSites:           // ---  Read API sites -----------------------------------------------------
                if (stateDebug) console.log('==> state machine - API SITES');
                activity = states.wait;  // Force next state to wait until response change that

                // Retrieve sites information
                await api.getSites(apiDebug,
                    dflt.retApiEndpoint(),
                    apiToken)
                    .then(response => {
                        // convert answer in obj to easy extract data
                        let objSite = JSON.parse(response);
                        if (mainDebug) {
                            console.log("Site data size  : " + objSite.Data.length);
                            console.log("Number of sites : " + objSite.Total)
                        }

                        localStorage.set('sites', objSite);         // Save the main object

                        for (var x in objSite.Data) {
                            if (mainDebug) {
                                console.log("Site [" + x + "] --> " + JSON.stringify(objSite.Data[x]));
                            }
                            localStorage.set('site' + x, objSite.Data[x]);    // Save the specific site
                        }

                        console.info("Retrieved " + localStorage.get('sites').Total + " sites\n");

                        // Select what Inventory API to use for the next passage
                        // if dataSource true select state 6 (read Inventory detailed) if false select state 5
                        // (read INventory legacy)
                        activity = dataSource ? states.apiInventoryD : states.apiInventoryL;
                    }, reject => {
                        console.error("Unable to retrieve site list from Savant server ! No CSV file prepared ! - " + reject);
                        activity = states.Abort;
                    });
                break;

            case states.apiInventoryL:             // --- Read API Inventory Legacy ------------------------------------
                if (stateDebug) console.log('==> state machine - API INVENTORY LEGACY');
                activity = states.wait;  // Force next state to wait until response change that

                {
                    // Start to retrieve data from identified sites
                    let siteProductsCount = 0;              // Counter for specific site (only for statistic/log)
                    let numberOfSites = localStorage.get('sites').Total;        // Retrieve number of sites available

                    let promiseCount = 0;                   // Counter used to be sure to change state only
                                                            // when all the promises are completed
                    for (let x = 0; x < numberOfSites; x++) {
                        promiseCount++;

                        let siteId = localStorage.get('site' + x).Id;
                        if (mainDebug) {
                            console.log("Retrieve inventory from site " + siteId);
                        }

                        // Uses get Inventory Details API
                        // Retrieve inventory details from site
                        await api.getInventory(apiDebug,
                            dflt.retApiEndpoint(),
                            apiToken,
                            siteId)
                            .then(response => {
                                if (promiseCount > 0) promiseCount--;
                                // convert answer in obj to easy extract data
                                let objInventory = JSON.parse(response);
                                if (mainDebug) {
                                    console.log("Inventory details for " + siteId + " : " + objInventory.length);
                                }

                                for (var y in objInventory) {
                                    if (mainDebug) {
                                        console.log("Product [" + totalProductsCount + "] --> " + JSON.stringify(objInventory[y]));
                                    }
                                    localStorage.set('rec' + totalProductsCount, objInventory[y]);     // Save the single inventory product
                                    totalProductsCount++;
                                    siteProductsCount++;
                                }
                                console.info("Retrieved " + siteProductsCount + " products details from " + siteId);
                                siteProductsCount = 0;      // Reset partial counter for next retrieval
                            }, reject => {
                                console.error("Unable to retrieve site list from Savant server ! No CSV file prepared ! - " + reject);
                                activity = states.Abort;
                            });
                    }
                    // Select if execute the UOM conversion
                    // if the UOM tconversion table exists, execute UOM conversion, otherwise skip to CSV preparation
                    if (promiseCount === 0) {
                        activity = uomConversionAvailable ? states.uomConv : states.csvPrep;

                        // If arrive here all the inventory data are locally stored - proceed to build CSV
                        console.info("Retrieved " + totalProductsCount + " products from the all sites\n")
                    } else {
                        console.warn("API Inventory Legacy - it should NEVER happens ! The lambda should abort in a while on the waiting state !");
                    }

                }
                break;

            case states.apiInventoryD:          // ---  Read API Inventory Detailed ------------------------------------
                if (stateDebug) console.log('==> state machine - API INVENTORY DETAILED');
                activity = states.wait;  // Force next state to wait until response change that

                {
                    // Start to retrieve data from identified sites
                    let siteProductsCount = 0;              // Counter for specific site (only for statistic/log)
                    let numberOfSites = localStorage.get('sites').Total;        // Retrieve number of sites available

                    let promiseCount = 0;                   // Counter used to be sure to change state only
                                                            // when all the promises are completed
                    for (let x = 0; x < numberOfSites; x++) {
                        promiseCount++;

                        let siteId = localStorage.get('site' + x).Id;
                        if (mainDebug) {
                            console.log("Retrieve inventory from site " + siteId);
                        }

                        // Uses get Inventory Details API
                        // Retrieve inventory details from site

                        await api.getInventoryDetails(apiDebug,
                            dflt.retApiEndpoint(),
                            apiToken,
                            siteId)
                            .then(response => {
                                if (promiseCount > 0) promiseCount--;
                                // convert answer in obj to easy extract data
                                let objInventory = JSON.parse(response);
                                if (mainDebug) {
                                    console.log("Inventory details for " + siteId + " : " + objInventory.length);
                                }

                                for (var y in objInventory) {
                                    if (mainDebug) {
                                        console.log("Product [" + totalProductsCount + "] --> " + JSON.stringify(objInventory[y]));
                                    }
                                    localStorage.set('rec' + totalProductsCount, objInventory[y]);     // Save the single inventory product
                                    totalProductsCount++;
                                    siteProductsCount++;
                                }
                                console.info("Retrieved " + siteProductsCount + " products details from " + siteId);
                                siteProductsCount = 0;      // Reset partial counter for next retrieval
                            }, reject => {
                                console.error("Unable to retrieve site list from Savant server ! No CSV file prepared ! - " + reject);
                                activity = states.Abort;
                            });
                    }

                    // Select if execute the UOM conversion
                    // if the UOM tconversion table exists, execute UOM conversion, otherwise skip to CSV preparation
                    if (promiseCount === 0) {
                        activity = uomConversionAvailable ? states.uomConv : states.csvPrep;

                        // If arrive here all the inventory data are locally stored - proceed to build CSV
                        console.info("Retrieved " + totalProductsCount + " products from the all sites\n")
                    } else {
                        console.warn("API Inventory Details - it should NEVER happens ! The lambda should abort in a while on the waiting state !");
                    }
                }
                break;

            case states.uomConv:            // --- UOM conversion ------------------------------------------------------
                if (stateDebug) console.log('==> state machine - UOM CONVERSION');

                var count = utils.uomConversion(dataSource, uomConversionTable, uomDebug);

                console.info('Applied UOM conversion to dataset - converted ' + count + ' records\n');
                activity = states.csvPrep;  // Force next state - prepare CSV file !
                break;

            case states.csvPrep:            // ---  CSV preparation  ---------------------------------------------------
                if (stateDebug) console.log('==> state machine - CSV PREPARATION');
                activity = states.wait;  // Force next state to wait until response change that

                await csv.prepareCSVFile(csvDebug,
                    dataSource)
                    .then(response => {
                        // CAUTION !
                        // Uncomment the printFile functions to see on the log the generated files
                        // Print generated file from CSV library
                        //printFile('/tmp/csvoutput.csv');

                        // Convert generated file using tab delimiter and write on final name
                        csv.convertCsvFile(csvDebug,
                            tempCsvResultFileName,
                            response,
                            dflt.retCsvDelimiter());

                        // CAUTION !
                        // Uncomment the printFile functions to see on the log the generated files
                        // Print final version CSV file
                        // printCsvFile(csvDebug, csvFilename);

                        generatedCsvFilename = response;        // Assign final CSV filename
                        console.info("Generated CSV file : " + generatedCsvFilename + "\n");

                        activity = states.sendS3;               // Force next state ! Send CSV file to S3 bucket
                    }, reject => {
                        console.error("Unable to prepare CSV file ! - " + reject);
                        activity = states.Abort;
                    });
                break;

            case states.sendS3:         // --- S3 bucket saving --------------------------------------------------------
                if (stateDebug) console.log('==> state machine - SENDING CSV TO S3');
                activity = states.wait;  // Force next state to wait until response change that

                // Upload file to S3 bucket
                await uploadFileS3(
                    dflt.retBucketName(),
                    dflt.retBucketRegion(),
                    generatedCsvFilename,
                    dflt.retCsvBucketPath())
                    .then(response => {
                        console.info(response);
                        activity = states.endOp;  // Force to exit gracefully
                    }, reject => {
                        console.error("Unable to send CSV file to S3 bucket ! - " + reject);
                        activity = states.Abort;
                    });
                break;

            case states.Abort:          // ---  ERROR ! Abort ! --------------------------------------------------------
                if (stateDebug) console.log('==> state machine - ABORT!');
                exit(-1);
                break;
        }   // exit state machine
    }   // exit loop

    console.info("Ending DailyInventorySnapshot");
}

//
// ------------------ S3 bucket operations  ------------------------------------------------------------
// Functions to store files in S3 bucket
//

const fs = require('fs');
const AWS = require('aws-sdk');
var path = require('path');

// Create Access for bucket
const s3 = new AWS.S3({
    accessKeyId: dflt.retAccessKey(),
    secretAccessKey: dflt.retAccessPwd()
});

// Function to upload a file in S3 bucket
// Parameters :
//  - name of the S3 bucket
//  - region where the bucket is located
//  - filename to upload (only name)
//  - path on S3 bucket where to store the file
const uploadFileS3 = (bucketName, inRegion, fileName, s3Path) => {
    return new Promise((resolve, reject) => {
        let s3FileName = s3Path + path.parse(fileName).base;

        // Set the Region
        //AWS.config.update({region: inRegion});

        // Read content from the file to be stored
        const fileContent = fs.readFileSync(fileName);

        console.log('CSV file will be stored on s3 : ' + s3FileName);

        // Setting up S3 upload parameters
        const params = {
            Bucket: bucketName,
            Key: s3FileName, // File name you want to save as in S3
            Body: fileContent
        };

        // Uploading files to the bucket
        s3.upload(params, function(err, data) {
            if (err) {
                throw err;
                reject (err);
            }
            resolve (`File uploaded successfully in S3. ${data.Location}`);
        });
    });
};

// Function to download  a file from S3 bucket
// Parameters :
//  - name of the S3 bucket
//  - region where the bucket is located
//  - filename to download (only name)
//  - path on S3 bucket where to retrieve the file
const downloadFileS3 = (bucketName, inRegion, fileName, s3Path) => {
    return new Promise((resolve, reject) => {
        let s3FileName = s3Path + path.parse(fileName).base;

        // Set the Region
        //AWS.config.update({region: inRegion});

        // Setting up S3 download parameters
        const params = {
            Bucket: bucketName,
            Key: s3FileName // File name you want to read from S3
        };

        // Downloading file from the bucket
        s3.getObject(params, function(err, data) {
            if (err) {
                console.error("Conversion file " + s3FileName + " on " + bucketName + " NOT found");
                reject ("Unable to find the file " + err);
            } else {
                // Save the CSV file retrieved locally
                fs.writeFile(localUomCsvFileName, data.Body, function (err) {
                    if (err) {
                        console.error("Conversion file " + s3FileName + " on " + bucketName + " can not be copied locally");
                        reject ("Unable to copy the file locally " + err);
                    }
                });

                const csvParse = require('csv-parser')
                // csvAnswer contains the file read from the S3 bucket.
                // Convert it and save in local storage - if the UOM tconversion table is not available the default is
                // to ignore it

                try {
                    fs.createReadStream(localUomCsvFileName)
                        .pipe(csvParse({separator: ','}))
                        .on('data', (data) => {
                            if (uomDebug) {
                                console.log("pushing : " + JSON.stringify(data));
                            }
                            uomConversionTable.push(data);
                        })
                        .on('end', () => {
                            if (uomDebug) {
                                console.log(uomConversionTable.length + " conversion data extracted and saved in array");
                            }
                        });
                } catch (e) {
                    console.error("Conversion file " + s3FileName + " on " + bucketName + " has errors");
                    reject ("Array preparation error")
                }

                console.info("Found and loaded UOM conversion file " + s3FileName + " on " + bucketName);
                resolve("Ok");
            }
        });
    });
};
