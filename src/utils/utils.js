//
// ------------------ Utilities ------------------------------------------------------------
//
let localStorage = require('store');

// Return true if the lambda is running out of time
// The function is used in the state machine wait state to force the application out of its misery in case
// of problem.
// It returns true if the application has left 150 ms of available time
function isTimeToGoodBye (context) {
    return ((context.getRemainingTimeInMillis()) > 150 ? false : true);
}

function uomConversion(dataSource, uomConversionArray, uomDebug) {
    // Notes !
    // If the data are retrieved from legacy API the quantity is present in QtyAvailable key
    // If the data are retrieved from detailed API the quantity is present in Qty key
    // All the retrieved data are available in the localStorage under the key "rec"
    // The conversion when applied keep 4 decimals
    // Use the dataSource flag in order to retrieve the correct Quantity

    let quantity    = 0;
    let adjustment  = 0.0;
    let index       = 0;
    let modifyCount = 0;

    if (uomDebug) {
        console.log(uomConversionArray.length + " entries in UOM conversion array");
    }
    try {
        localStorage.each(function (value, key) {
            if (key === 'rec'+index) {
                // Check if the product exists in the UOM conversion table
                for (let i=0; i < uomConversionArray.length; i++) {
                    if (uomConversionArray[i]["ProductId"].localeCompare((value.ProductID).trimEnd()) === 0) {
                        // Found !   Retrieve quantity !! Careful
                        quantity = dataSource ? value.Qty : value.QtyAvailable;
                        adjustment = uomConversionArray[i]["UOM Conversion"];

                        if (uomDebug) {
                            console.log("Found product " +
                                (value.ProductID).trimEnd() +
                                " in UOM conversion table - original Quantity : " +
                                quantity +
                                " multiply by " +
                                adjustment);
                        }
                        quantity = (quantity * adjustment).toFixed(4);
                        if (uomDebug) {
                            console.log("Updated result : " + quantity);
                        }

                        if (dataSource) {
                            value.Qty = quantity;
                        } else {
                            value.QtyAvailable = quantity;
                        }

                        // Need to write it back on localStorage
                        localStorage.set(key, value);
                        modifyCount++;      // Increase counter of modified records
                        break;
                    }
                }
                index++;
            }
        })
    } catch (e) {
        console.error("UOM conversion error ! -  " + e);
    }
    return modifyCount;
}

module.exports = { isTimeToGoodBye, uomConversion };