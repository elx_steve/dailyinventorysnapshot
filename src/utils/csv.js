//
// ------------------ CSV operations  ------------------------------------------------------------
// Functions to handle CSV files
//   - it uses the csv-writer library (https://www.npmjs.com/package/csv-writer) to write the CSV
//   - it uses the csv-parse library (https://www.npmjs.com/package/csv-parser) to read the CSV
//
let localStorage = require('store');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

// CSV required format :
// Key                  Description
//----------------------------------------------------------------------------
// As of Date         → Current date (format mm/dd/yyyy )
// Rec ID             → null
// Source System      → 16
// Item Id            → retrieved from Inventory API : ProductID
// Inventory Type     → 1
// Location ID        → retrieved from Inventory API : code converted from siteID
// lot number         → retrieved from Inventory API : lotNumber
// units              → retrieved from Inventory API : Quantity
// package id         → null
// strain nm          → null
// source facility nm → null
// dest facility desc → null
// po create date     → null
// room id            → null
// testing status     → null
// additional info    → retrieved from Inventory API : Description
// expiration date    → retrieved from Inventory API : CodeDate (format yyy-mm-ddThh:MM:ss)
// subroom            → null
//
// CSV preparation stages :
//   - create file - format header in a file called /tmp/csvoutput.cvs
//   - add retrieved data to the created file
//   - write created CSV file
//   - convert CSV created file in final CSV file changing the delimiter
//   - write file with final official name
// Filename : savant_inventoryfeed_<date-timestamp>.csv
let csvFilename = "";
// Delimiter : Tab
let delimiter = '\t';

let csvDebug = false;       // Set variable here - is updated from the public functions

// --------------------- Public functions -------------------------------------------------------------

// Prepare CSV file
// The function require as input :
//  - debug flag to show CSV related log messages
//  - type source input (legacy vs detailed) :
//    false : legacy
//    true  : detailed
function prepareCSVFile(csvDebugIn,
                        dataSource) {
    csvDebug = csvDebugIn;

    return new Promise((resolve, reject) => {
        if (csvDebug) {
            console.info("Preparing CSV file")
        }

        // Preparing the date to be used in the file name and in the file content
        let currentCsvDate = new Date();
        // Format on standard datetime format : yyyy-mm-ddThh:MM:ss
        let formatted_date = currentCsvDate.getFullYear() + "-" +
            (appendLeadingZeroes(currentCsvDate.getMonth() + 1)) + "-" +
            appendLeadingZeroes(currentCsvDate.getDate()) + "T" +
            appendLeadingZeroes(currentCsvDate.getHours()) + ":" +
            appendLeadingZeroes(currentCsvDate.getMinutes()) + ":" +
            appendLeadingZeroes(currentCsvDate.getSeconds());

        // Complete the file name to be written
        // Is used in the last conversion stage
        csvFilename = retCsvBaseFilename() + formatted_date + ".csv";

        const csvValues = [];   // Array containing the values to be put in the CSV file
        const csvFile = formatResultHeader();  // Prepare the CSV file header

        // Filling up with data retrieved
        fillUpCsvFile(formatted_date, csvValues, dataSource);

        if (csvDebug) {
            console.info("Writing CSV file")

//            console.log("Generated header : " + JSON.stringify(csvFile) + "\n\n");
//            console.log("Generated values : " + JSON.stringify(csvValues));
        }
        // Prepare the CSV file

        csvFile
            .writeRecords(csvValues)
            .then(() => {
                if (csvDebug) {
                    console.log('CSV File Created.');
                }

                // Return the CSV filename generated
                resolve(csvFilename);
            })
            .catch(err => {
                console.error("writeRecords failed : " + err);
                reject("Error : " + err);
            })
    });
}

// Convert comma with tab in a CSV file
function convertCsvFile(csvDebugIn,
                        inFilename,
                        outFilename,
                        separator) {

    csvDebug = csvDebugIn;

    if (csvDebug) {
        console.log("Convert " + inFilename + " to " + outFilename);
        if (separator === ',') {
            console.log("The CSV delimiter requested is the same already used - NO CONVERSION necessary.\n Just rename the file" + inFilename + " to " + outFilename);
        }
    }

    const fs = require('fs');

    try {
        // read contents of the file
        const contentIn = fs.readFileSync(inFilename, 'UTF-8');

        // split the contents by new line
        let inLines = contentIn.split(/\r?\n/);

        // print all lines
        let line1 = "";
        let lineCounter = 0;        // Counter for generated lines
        inLines.forEach((line) => {
            if (line) {
                // The generated CSV file is comma delimited. If the chosen delimiter is comma, just copy
                // the lines as is, if not convert them
                if (separator === ',') {
                    line1 = line;
                } else {
                    line1 = line.replace(/,/gi, separator);
                }
                // Write the data
                fs.writeFileSync(outFilename,
                    line1 + "\n",
                    {
                        encoding: "utf8",
                        flag: "a+",
                        mode: 0o666
                    });
                lineCounter++;
            }
        });
        // Report in the log
        console.info("File " + outFilename + " prepared - it contains " + (lineCounter - 1) + " records");

    } catch (err)
    {
        console.error(err);
    }
}

// Printing out a file on a log
function printCsvFile(filename) {
    if (csvDebug) {
        console.log("Verification CSV file prepared : " + filename);
    }
    process.stdout.write("--------------------------------------\n");

    const fs = require('fs');

    try {
        // read contents of the file
        const data = fs.readFileSync(filename, 'UTF-8');

        // split the contents by new line
        const lines = data.split(/\r?\n/);

        // print all lines
        lines.forEach((line) => {
            process.stdout.write(line + "\n");
        });
        process.stdout.write("--------------------------------------\n");

    } catch (err) {
        console.error(err);
    }
}

// --------------------- Private functions ------------------------------------------------------------

// Return file name
// The base is not defined as global variable
function retCsvBaseFilename() {
    return "/tmp/savant_inventoryfeed_";
}

// Prepare the base for the result CSV file
function formatResultHeader() {
    const csvWriter = createCsvWriter({
        path: '/tmp/csvoutput.csv',
        header: [
            {
                id: 'as_of_date',
                title: 'as_of_dt'
            },
            {
                id: 'rec_id',
                title: 'rec_id'
            },
            {
                id: 'source_system',
                title: 'source_system_id'
            },
            {
                id: 'item_id',
                title: 'item_id'
            },
            {
                id: 'inventory_type',
                title: 'inventory_type'
            },
            {
                id: 'location_id',
                title: 'location_id'
            },
            {
                id: 'lot_number',
                title: 'lot_number'
            },
            {
                id: 'units',
                title: 'units'
            },
            {
                id: 'package_id',
                title: 'package_id'
            },
            {
                id: 'strain_nm',
                title: 'strain_nm'
            },
            {
                id: 'source_facility_nm',
                title: 'source_facility_nm'
            },
            {
                id: 'dest_facility_desc',
                title: 'dest_facility_desc'
            },
            {
                id: 'po_create_date',
                title: 'po_create_da'
            },
            {
                id: 'room_id',
                title: 'room_id'
            },
            {
                id: 'testing_status',
                title: 'testing_status'
            },
            {
                id: 'additional_info',
                title: 'additional_info'
            },
            {
                id: 'expiration_date',
                title: 'expiration_dt'
            },
            {
                id: 'subroom',
                title: 'subroom'
            }
        ]
    });
    return csvWriter;
}

// The function fills up the values fro the CSV file
// It requires :
//  - the date to be used
//  - csvContent  (raw data acquired)
//  - dataSource  (false -> Legacy - true -> detailed)
function fillUpCsvFile(formatted_date,
                       csvContent,
                       dataSource) {
    let index=0;
    if (csvDebug) {     // TEMP TEMP
        console.log("Filling up data retrieved from " + (dataSource ? "detailed" : "legacy") + " Inventory API in CSV file");
    }

    localStorage.each(function(value, key) {
        if(key === 'rec'+index) {
            // Preparing value to put in the CSV file
            let productId = (value.ProductID).trimEnd();
            let lotNumber = value.LotNum;
            let description = (value.Description).trimEnd();
            let codeDate = value.CodeDate;
            let uom = (value.UOM).trimEnd();
            let siteId = (value.SiteID).trimEnd();

            // Set legacy values on some fields - can be overwritten if deatild info are available
            let packageId    = "";
            let roomId       = "";
            let subRoom      = "";
            let qtyAvailable = "";

            if (dataSource) {
                // Data extracted from detailed Inventory API
                packageId = value.LicensePlate;
                roomId = (value.LocationID).charAt(0);
                qtyAvailable = value.Qty;
                subRoom = value.LocationID;
            } else {
                // Data extracted from Legacy Inventory API
                qtyAvailable = value.QtyAvailable;
            }

            if (csvDebug) {
                console.log("Process product ID [" + productId + "] - index " + index);
                console.log("lotNumber      : " + lotNumber);
                console.log("qtyAvailable   : " + qtyAvailable);
                console.log("description    : " + description);
                console.log("codeDate       : " + codeDate);
                console.log("siteId (coded) : " + codingSiteId(siteId));
                console.log("packageId      : " + packageId);
                console.log("roomId         : " + roomId);
                console.log("subRoom        : " + subRoom);
                console.log("---------------------------------------");
            }

            var product = {
                as_of_date: formatted_date,
                rec_id: "",
                source_system: "16",
                item_id: productId,
                inventory_type: "1",
                location_id: codingSiteId(siteId),
                lot_number: lotNumber,
                units: qtyAvailable,
                package_id: packageId,
                strain_nm: "",
                source_facility_nm: "",
                dest_facility_desc: "",
                po_create_date: "",
                room_id: roomId,
                testing_status: "",
                additional_info: description,
                expiration_date: codeDate,
                subroom: subRoom
            }

            csvContent.push(product);
            index++;
        }
    })
}

// Function to convert siteID in code
// NOTE !! At this moment the conversion is hardcoded !
// It would be better to use the data retrieved from API getSites but at this moment (Oct 2020) the
// converted codes ARE NOT THERE !!!
function codingSiteId(siteId) {
    switch (siteId) {
        case 'MAIN':
        case 'TAMPA':
            return 3;
            break;
        case 'WIMAUMA':
            return 69;
            break;
        case 'SUNSHINE':
            return 105;
            break;
        default:
            return 0;
            break;
    }
}

// Adding leading 0 if missing in date/time values
function appendLeadingZeroes(n){
    if(n <= 9){
        return "0" + n;
    }
    return n
}

module.exports = { prepareCSVFile, convertCsvFile, printCsvFile };