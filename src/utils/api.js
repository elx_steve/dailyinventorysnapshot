//
// ------------------ API interface ------------------------------------------------------------
// Savant API interface
//  - getToken     -> retrieve access token
//  - getSites     -> retrieve valid sites
//  - getInventory -> retrieve inventory from a specific site
//
const request = require('request-promise');
const qs = require('querystring');

// getToken using 'request' from Postman example
// The function return a promise
function getToken(apiDebug, apiEndpoint, siteId) {
    let bodyData = qs.stringify({
        username: 'APIUSER',
        password: 'seK#rit2020!',
        grant_type: 'password',
        siteid: siteId
    });

    let options = {
        url: apiEndpoint + "/oauth/token",
        method: 'POST',
        preambleCRLF: true,
        postambleCRLF: true,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ZDE0YTQ5NzMtODhkZC00YzQ0LTk1NDMtMGMxYTk4NWJmY2MwOkYvcGkwUUYveFZlLzJlcklGcDFBcXRMZWRlRW1TelRrREM3ckU3bzRscW89'
        },
        form: bodyData
    }

    return new Promise((resolve, reject) => {
        if (apiDebug) console.log("getToken function called");

        request(options, function(err, response, body) {
            if (response) {
                let respObj = JSON.parse(JSON.stringify(response));
                switch (respObj.statusCode) {
                    case 200:   // OK
                        if (apiDebug) console.log("200 OK");
                        break;
                    default:
                        if (apiDebug) console.log(respObj.statusCode + " ERROR");
                        reject ("Response error : " + respObj.statusCode);
                        break;
                }
            }

            if (err) {
                reject ("Error from getToken : " + err);
            } else {
                if (body) {
                    if (apiDebug) console.log("Valid Answer from getToken : " + body);
                    resolve(body);
                } else {
                    reject ("No data retrieved from getToken !");
                }
            }
        });
    });
}

// getSites using 'request' from Postman example
// The function return a promise
function getSites(apiDebug, apiEndpoint, access_token) {
    let options = {
        'method': 'GET',
        'url': apiEndpoint + '/api/whssite/getallwhssiteforlookup',
        'headers': {
            'Authorization': 'Bearer ' + access_token
        }
    };

    return new Promise((resolve, reject) => {
        if (apiDebug) console.log("getSite function called");

        request(options, function(err, response, body) {
            if (response) {
                let respObj = JSON.parse(JSON.stringify(response));
                switch (respObj.statusCode) {
                    case 200:   // OK
                        if (apiDebug) console.log("200 OK");
                        break;
                    default:
                        if (apiDebug) console.log(respObj.statusCode + " ERROR");
                        reject ("Response error : " + respObj.statusCode);
                        break;
                }
            }

            if (err) {
                reject("Error from getSites : " + err);
            } else {
                if (body) {
                    if (apiDebug) console.log("Valid Answer from getSites : " + body);
                    resolve(body);
                } else {
                    reject("No data retrieved from getSites !");
                }
            }
        });
    });
}

// getInventory using 'request' from Postman example
// The function return a promise
function getInventory(apiDebug, apiEndpoint, access_token, site) {
    let options = {
        'method': 'GET',
        'url': apiEndpoint + '/api/inventory/getCurrentInventoryForOwner?siteid=' + site + '&ownerid',
        'headers': {
            'Authorization': 'Bearer ' + access_token
        }
    };

    return new Promise((resolve, reject) => {
        if (apiDebug) console.log("getInventory function called");

        request(options, function(err, response, body) {
            if (response) {
                let respObj = JSON.parse(JSON.stringify(response));
                switch (respObj.statusCode) {
                    case 200:   // OK
                        if (apiDebug) console.log("200 OK");
                        break;
                    default:
                        if (apiDebug) console.log(respObj.statusCode + " ERROR");
                        reject ("Response error : " + respObj.statusCode);
                        break;
                }
            }

            if (err) {
                reject("Error from getInventory : " + err);
            } else {
                if (body) {
                    if (apiDebug) console.log("Valid Answer from getInventory : " + body);
                    resolve (body);
                } else {
                    reject("No data retrieved from getInventory !");
                }
            }
        });
    });
}

// getInventory using 'request' from Postman example
// The function return a promise
function getInventoryDetails(apiDebug, apiEndpoint, access_token, site) {
    let options = {
        'method': 'GET',
        'url': apiEndpoint + '/api/reports/InventoryDetailByProduct?ownerid&siteid=' + site,
        'headers': {
            'Authorization': 'Bearer ' + access_token
        }
    };

    return new Promise((resolve, reject) => {
        if (apiDebug) console.log("getInventoryDetail function called");

        request(options, function(err, response, body) {
            if (response) {
                let respObj = JSON.parse(JSON.stringify(response));
                switch (respObj.statusCode) {
                    case 200:   // OK
                        if (apiDebug) console.log("200 OK");
                        break;
                    default:
                        if (apiDebug) console.log(respObj.statusCode + " ERROR");
                        reject ("Response error : " + respObj.statusCode);
                        break;
                }
            }

            if (err) {
                reject("Error from getInventoryDetail : " + err);
            } else {
                if (body) {
                    if (apiDebug) console.log("Valid Answer from getInventoryDetail : " + body);
                    resolve (body);
                } else {
                    reject("No data retrieved from getInventoryDetail !");
                }
            }
        });
    });
}

module.exports = { getToken, getSites, getInventory, getInventoryDetails };