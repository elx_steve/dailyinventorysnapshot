// Functions to return the system assignment variables
// The functions return a default value if the global system variable is NOT defined, otherwise are
// returning the value defined in the global system variable.

// Variables handled
//  - SAV_BUCKET                    --> bucket name to use
//  - SAV_BUCKET_REGION             --> region where the bucket exists (currently not used)
//  - SAV_ACCESS_KEY                --> Access key for lambda environment
//  - SAV_ACCESS_PWD                --> Access key password for lambda environment
//  - SAV_API_ENDPOINT              --> API endpoint to use
//  - SAV_API_SITEID_LOGIN          --> SiteID to be used for login
//  - SAV_API_INVENTORY_DETAIL_FEED --> if set to true uses new detailed API for Inventory retrieval
//  - SAV_CSV_PATH                  --> Path where to save result CSV file on S3 bucket
//  - SAV_CSV_PATH_UOMCONV          --> Path where to find UOM conversion CSV file
//  - SAV_CSV_DELIMITER             --> CSV delimiter to use to generate result CSV file (NOT Used for UOM conversion CSV file)
//  - SAV_DEBUG                     --> Debug value - see documentation for details

// Return lambda version (information only - for now manually updated !)
function retLambdaVersion () {
    return "0.1.2";
}
// Return bucket name
function retBucketName () {
    return (process.env.SAV_BUCKET) ? process.env.SAV_BUCKET : "savant-test-bucket";
}
// Return bucket region
function retBucketRegion () {
    return (process.env.SAV_BUCKET_REGION) ? process.env.SAV_BUCKET_REGION : "us-east-1";
}
// Return access key
function retAccessKey () {
    return (process.env.SAV_ACCESS_KEY) ? process.env.SAV_ACCESS_KEY : "AKIA5UK4KR7UWGLOJ2RC";
}
// Return access password
function retAccessPwd () {
    return (process.env.SAV_ACCESS_PWD) ? process.env.SAV_ACCESS_PWD : "6zDU/FZTuUbFPFJo02dQQ6dlKul0RkCtvesHDJ96";
}
// Return API endpoint
function retApiEndpoint () {
    return (process.env.SAV_API_ENDPOINT) ? process.env.SAV_API_ENDPOINT : "https://api.wms.aws.liveparallel.com";
}
// Return SiteID to be used with the login
function retApiSiteIdLogin () {
    return (process.env.SAV_API_SITEID_LOGIN) ? process.env.SAV_API_SITEID_LOGIN : "TAMPA";
}
// Return what API to use for retrieve inventory
function retApiInventoryDetails () {
    return (process.env.SAV_API_INVENTORY_DETAIL_FEED) ? process.env.SAV_API_INVENTORY_DETAIL_FEED : "true";
}
// Return S3 bucket path where to save result CSV file
function retCsvBucketPath () {
    return (process.env.SAV_CSV_PATH) ? process.env.SAV_CSV_PATH : "";
}
// Return S3 bucket path for UOM conversion CSV file
function retCsvBucketPathUomConversion () {
    return (process.env.SAV_CSV_PATH_UOMCONV) ? process.env.SAV_CSV_PATH_UOMCONV : "uom_conversion/";
}
// Return CSV delimiter
function retCsvDelimiter () {
    return (process.env.SAV_CSV_DELIMITER) ? process.env.SAV_CSV_DELIMITER : "\t";
}
// Return debug setting
function retDebug () {
    return (process.env.SAV_DEBUG) ? process.env.SAV_DEBUG : "1";
}
// Display content of variable value assignment
// CAUTION !  Remember to add any new variable display here !!
function displayGlobalAssignment() {
    console.log('------ System variables -- Vs: ' + retLambdaVersion() + ' ---------------------');
    switch (retDebug()) {
        case '0':           // No debug
        default:
            break;
        case '1':
            console.log('Debug                    => Only System messages');
            break;
        case '2':
            console.log('Debug                    => Only Main messages');
            break;
        case '3':
            console.log('Debug                    => Only API messages');
            break;
        case '4':
            console.log('Debug                    => Only CSV messages');
            break;
        case '5':
            console.log('Debug                    => Only UOM messages');
            break;
        case '6':
            console.log('Debug                    => Only State machine');
            break;
        case '7':
            console.log('Debug                    => ALL messages enabled');
            break;
    }
    console.log('Bucket name              =>', retBucketName());
    console.log('Bucket region            =>', retBucketRegion());
    console.log('Bucket access key        =>', retAccessKey());
    console.log('Bucket access pwd        =>', retAccessPwd());
    console.log('API endpoint             =>', retApiEndpoint());
    console.log('API SiteID login         =>', retApiSiteIdLogin());
    console.log('API Inventory used       =>', (retApiInventoryDetails().localeCompare("true") === 0) ? "Detailed" : "Legacy");
    console.log('Bucket CSV path          =>', retCsvBucketPath());
    console.log('Bucket UOM Conv CSV path =>', retCsvBucketPathUomConversion());
    console.log('CSV delimiter            =>', retCsvDelimiter() === '\t' ? 'Tab' : retCsvDelimiter() + "\n" );
}

// Exporting modules area
module.exports = {
    retBucketName, retBucketRegion, retAccessKey, retAccessPwd, retApiEndpoint, retApiSiteIdLogin,
    retApiInventoryDetails, retCsvBucketPath, retCsvBucketPathUomConversion, retCsvDelimiter, retDebug,
    displayGlobalAssignment
}